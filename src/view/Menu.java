package view;

import controller.GameCtrl;

import javax.swing.*;
import java.io.IOException;

public class Menu extends JFrame {
    private GameCtrl gameCtrl;
    public Menu(GameCtrl gameCtrl) {
        this.gameCtrl = gameCtrl;
        this.setTitle("Need For Speed On The Ground");
        this.setSize(800, 400);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel selectorsPnl = new JPanel();
        this.add(selectorsPnl);
        JButton startBtn = new JButton("Start game !");
        startBtn.addActionListener(e -> {
            try {
                gameCtrl.startGame();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
        JButton closeBtn = new JButton("Close");
        closeBtn.addActionListener(ev -> {
            this.dispose();
        });
        selectorsPnl.add(startBtn);
        selectorsPnl.add(closeBtn);
        this.setVisible(true);
    }
}
