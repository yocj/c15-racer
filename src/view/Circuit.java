package view;

import model.Car;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Circuit extends JFrame {
    private int xPos = 0;
    private int yPos = 0;
    private Car car;
    public Circuit(Car car) {
        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                xPos = e.getX();
                yPos = e.getY();
                repaint();
            }
        });
        this.car = car;
        this.setTitle("Race");
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel circuitPnl = new JPanel() {
        @Override
            protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.drawImage(car.getCarImage(), (int) car.getPosition().x, (int) car.getPosition().y, null);
        }
        };
        this.add(circuitPnl);
        this.setVisible(true);
    }
}
