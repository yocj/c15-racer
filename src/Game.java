import controller.GameCtrl;
import view.Menu;

public class Game {
    public static void main(String[] args) {
        GameCtrl gameCtrl = new GameCtrl();
        gameCtrl.initUI();
    }
}
