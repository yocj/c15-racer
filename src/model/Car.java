package model;

import javax.imageio.ImageIO;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Car {
    private Point2D.Double position;
    private Point2D.Double velocity;
    private double angle;
    private int length;
    private int maxAcceleration;
    private int maxSteering;
    private double currentAcceleration;
    private double currentSteering;
    private BufferedImage carImage;

    public Car(int length, int maxAcceleration, int maxSteering) throws IOException {
        this.position = new Point2D.Double(0,0);
        this.velocity = new Point2D.Double(0,0);
        this.angle = 0;
        this.length = length;
        this.maxAcceleration = maxAcceleration;
        this.maxSteering = maxSteering;
        this.currentAcceleration = 0;
        this.currentSteering = 0;
        this.carImage = ImageIO.read(new File("./res/car.png"));
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public void setCurrentAcceleration(double currentAcceleration) {
        this.currentAcceleration = currentAcceleration;
    }

    public void setCurrentSteering(double currentSteering) {
        this.currentSteering = currentSteering;
    }

    public Point2D.Double getPosition() {
        return position;
    }

    public double getAngle() {
        return angle;
    }

    public double getCurrentAcceleration() {
        return currentAcceleration;
    }

    public double getCurrentSteering() {
        return currentSteering;
    }

    public void setPosition(Point2D.Double position) {
        this.position = position;
    }

    public BufferedImage getCarImage() {
        return this.carImage;
    }

    public Point2D.Double getVelocity() {
        return velocity;
    }

    public void setVelocity(Point2D.Double velocity) {
        this.velocity = velocity;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getMaxAcceleration() {
        return maxAcceleration;
    }

    public void setMaxAcceleration(int maxAcceleration) {
        this.maxAcceleration = maxAcceleration;
    }

    public int getMaxSteering() {
        return maxSteering;
    }

    public void setMaxSteering(int maxSteering) {
        this.maxSteering = maxSteering;
    }

    public void setCarImage(BufferedImage carImage) {
        this.carImage = carImage;
    }
}
