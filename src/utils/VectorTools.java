package utils;

import java.awt.geom.Point2D;

public class VectorTools {
    public static Point2D.Double rotate(Point2D vector, double angle, double dt) {
        double newX = vector.getX() * Math.cos(angle) - vector.getY() * Math.sin(angle);
        double newY = vector.getX() * Math.sin(angle) + vector.getY() * Math.cos(angle);
        return new Point2D.Double(newX * dt, newY * dt);
    }
}
