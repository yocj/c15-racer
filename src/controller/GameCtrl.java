package controller;

import model.Car;
import utils.VectorTools;
import view.Circuit;
import view.Menu;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Point2D;
import java.io.IOException;

public class GameCtrl implements KeyListener {
    private Menu menu;
    private Circuit circuit;
    private Car car;
    private boolean isPlaying;
    private long timeStart;
    private double time;
    private double lastTime;
    private double dt;
    private Thread gameThread;
    public GameCtrl() {
        this.isPlaying = false;
        this.time = 0;
        this.lastTime = 0;
        this.dt = 0;
    }
    public void initUI() {
        this.menu = new Menu(this);
    }
    public void startGame() throws IOException {
        this.car = new Car(2, 30, 45);
        this.circuit = new Circuit(this.car);
        this.circuit.addKeyListener(this);
        this.isPlaying = true;
        this.timeStart = System.nanoTime();
        this.gameThread = new Thread(this::gameLoop);
        this.gameThread.start();
    }

    private void gameLoop() {
        while (isPlaying) {
            this.time = (System.nanoTime() - timeStart);
            this.dt = this.time - this.lastTime;
            this.lastTime = this.time;
            this.update(this.dt);
            circuit.repaint();
        }
    }

    private void update(double dt) {
        car.setVelocity(new Point2D.Double(car.getCurrentAcceleration() * dt, 0));
        System.out.println(VectorTools.rotate(car.getVelocity(), car.getAngle(), dt));
        car.setPosition(VectorTools.rotate(car.getVelocity(), -car.getAngle(), dt));
    }

    @Override
    public void keyTyped(KeyEvent e) {
        switch (e.getKeyChar()) {
            case 'z':
                System.out.println("z");
                car.setCurrentAcceleration(car.getCurrentAcceleration() + 1 * this.dt);
                break;
            case 's':
                break;
            case 'q':
                break;
            case 'd':
                break;
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
